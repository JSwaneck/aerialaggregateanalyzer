# Aerial Aggregate Analyzer

Aerial Aggregate Analyzer - ECE 480 Capstone Spring 2020

This repo contains two project, a **Desktop GUI** and a **Pi Web Server**

Designed to control a remote Raspberry PI over IP 

## Getting Started

To set up a fresh Raspberry PI with the Aerial Aggregate Analyzer server

1. Setup pi with Raspbian Buster No Desktop (desktop version untested but may work)

2. Enable SSH to remotely connect, take note of your IP for later

3. Enable the camera [Guide](https://www.raspberrypi.org/documentation/usage/camera/)

4. Plug in a motor to port 18

5. Ensure pip is installed, if not run:
    1. sudo apt-get update
    2. sudo apt-get install python-pip

6. Add this line to /etc/sudoers
    1. Defaults env_keep += "PYTHONPATH"

7. Run these commands
    1. sudo pip install flask
    2. sudo pip install futures
    3. 
    4. Note: May have missed one or two, if you see "No module named ***" then you'll need to install another

8. Download the Raspberry Pi server from this current [repo](https://bitbucket.org/JSwaneck/aerialaggregateanalyzer/src/master/)
You'll only need the .py files under RaspberryPi, place them on the Pi

9. Download the desktop GUI to a PC or Mac. Same repo, you'll only need the DesktopGui files

10. All setup! Time to run everything

## Running Everything

1. Run the web server on the Pi, starting the pigpio daemon first
    1. sudo pigpiod
    2. sudo python app.py

2. Run the desktop GUI on a laptop or computer on the same network

3. Enter the Pi's LAN IP address you noted in step 2


## Credits
- Jacob Swaneck (jpswaneck@gmail.com)
- Audrey Guest (guestaud@msu.edu)
- Megan Henderson (hende405@msu.edu)
- Anna Schierlinger (schierl2@msu.edu)
- Anna De Biasi (debiasi1@msu.edu)
- Cory Hilton (hiltonc2@msu.edu)
