import RPi.GPIO as GPIO
import time
import pigpio
import os
import datetime
from Camera import Camera
from picamera import PiCamera

PIN = 18         # Servo motor pin, must use GPIO w/ hardware PWM support. 18 is default
MIN_PW = 1434.5  # Minimum pulse width to move motor
STOP_PW = 1500   # Pulse width to stop motor
STEP_LEN = 0.1   # Adjusts spacing between pictures
STEPS = 90       # Adjusts total steps


def rotate_camera_take_pictures(directory, cache):
        if cache['current-rotating']:
            return
        cache['current-rotating'] = True
        cache['current-pictures-total'] = STEPS
        cache['current-pictures-taken'] = 0
        pictures_taken = 0
        # Initialize pigpioi
        p = pigpio.pi()
        p.set_mode(PIN, pigpio.OUTPUT)

        # This moves the tray one step
        def step_motor():
            p.set_servo_pulsewidth(PIN, MIN_PW)
            time.sleep(STEP_LEN)
            p.set_servo_pulsewidth(PIN, STOP_PW)

        time.sleep(3)
        camera = Camera()

        # Move servo and capture pictures
        for i in range(STEPS):
            step_motor()
            time.sleep(0.5)
            with open(directory + '/image%s.jpg' % i, 'wb') as out:
                out.write(camera.get_frame())
            pictures_taken += 1
            cache['current-pictures-taken'] = pictures_taken

        # Stop servo
        p.set_servo_pulsewidth(PIN, STOP_PW)
        p.stop()
        cache['current-rotating'] = False
