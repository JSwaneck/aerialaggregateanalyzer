from flask import Flask, render_template, Response, jsonify, send_from_directory
from Camera import Camera
import os
from concurrent.futures import ThreadPoolExecutor

import CaptureScript

executor = ThreadPoolExecutor(1)

app = Flask(__name__)

pictures_dir = os.path.join(app.instance_path, 'pictures')
if not os.path.isdir(pictures_dir):
    os.makedirs(pictures_dir)


def get_next_dir_num(num=1):
    while 1:
        d = os.path.join(pictures_dir, "Capture" + str(num))
        if not os.path.isdir(d):
            return num
        num += 1


starting_num = get_next_dir_num()
cache = {
    'pictures-dir': pictures_dir,
    'next-num': starting_num,
    'next-dir': os.path.join(pictures_dir, "Capture" + str(starting_num)),
    'current-dir': '',
    'current-rotating': False,
    'current-pictures-total': -1,
    'current-pictures-taken': -1,
    'connected': True
}

camera_cache = {
    'camera': None
}


@app.route('/', methods=['GET'])
def index():
    return '<h1>Aerial Aggregate Analyzer - RPi API</h1>'


@app.route('/ping')
def ping():
    return 'Hello World'


@app.route('/status', methods=['GET'])
def status():
    return jsonify(cache)


@app.route('/start_rotation')
def rotation():
    if not cache['current-rotating']:
        os.makedirs(cache['next-dir'])
        executor.submit(CaptureScript.rotate_camera_take_pictures, cache['next-dir'], cache)
        cache['current-dir'] = cache['next-dir']
        cache['next-num'] = cache['next-num'] + 1
        cache['next-dir'] = os.path.join(cache['pictures-dir'], "Capture" + str(cache['next-num']))
    return '{}'


@app.route('/get_capture_photo/<int:num>')
def get_capture_photo(num):
    return send_from_directory(cache['current-dir'], 'image' + str(num) + '.jpg', as_attachment=False)


def gen(camera):
    """Video streaming generator function."""
    while True:
        frame = camera.get_frame()
        yield (b'--frame\r\n'
               b'Content-Type: image/jpeg\r\n\r\n' + frame + b'\r\n')


@app.route('/video_feed')
def video_feed():
    """Video streaming route. Put this in the src attribute of an img tag."""
    # if camera_cache['camera'] is None:
    #     camera_cache['camera'] = Camera()
    return Response(gen(Camera()),
                    mimetype='multipart/x-mixed-replace; boundary=frame')

if __name__ == '__main__':
    app.run(debug=True, host='0.0.0.0')
