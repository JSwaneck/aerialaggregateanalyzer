/**
 * Handles data transfer to raspberry pi
 */

import {defaultRemoteStatus, IRemoteStatus, toIRemoteStatus} from '@/business/RemoteStatus';
import {HttpClientResponse} from 'typed-rest-client/HttpClient';
import * as rm from 'typed-rest-client/RestClient';
import * as fs from 'fs';

/**
 * Raspberry Pi Flask Extensions
 */
const STATUS_EXTENSION : string = '/status';
const VIDEO_STREAM_EXTENSION : string = '/video_feed';
const START_ROTATION_EXTENSION : string = '/start_rotation';
const GET_PHOTO_EXTENSION : string = '/get_capture_photo';

/**
 * Flle system
 */
const IMAGE_ROOT_DIR : string = '/Captures';
const IMAGE_SUBDIR_PREFIX : string = '/Capture';

/**
 * Handles data transfer to raspberry pi
 */
export class Connection {
    private remoteHost: string;
    private remotePort: string;
    private currentRemoteStatus : IRemoteStatus;
    private picturesDownloaded : number;
    private pingConnected: boolean;
    private connectionError: boolean;
    private restClient : rm.RestClient | null;
    private nextCaptureDirNum : number;

    public constructor(remoteHost: string, remotePort: string) {
        this.remoteHost = remoteHost;
        this.remotePort = remotePort;
        this.pingConnected = false;
        this.connectionError = false;
        this.currentRemoteStatus = defaultRemoteStatus();
        this.picturesDownloaded = 0;
        this.updateRestClient();
        this.nextCaptureDirNum = 0;
    }

    public setRemote(newHost: string, newPort: string) : void{
        this.remoteHost = newHost;
        this.remotePort = newPort;
        this.updateRestClient();
    }

    public setRemoteHost(newHost: string) : void {
        this.remoteHost = newHost;
        this.updateRestClient();
    }
    public getRemoteHost() : string {
        return this.remoteHost;
    }
    public setRemotePort(newPort: string) : void {
        this.remotePort = newPort;
        this.updateRestClient();
    }
    public getRemotePort() : string {
        return this.remotePort;
    }
    public connected() : boolean {
        return this.pingConnected;
    }
    public invalidConnection() : boolean {
        return this.connectionError;
    }
    public getPicturesDownloaded() : number {
        return this.picturesDownloaded;
    }
    public getVideoStreamSrc() : string {
        return this.baseUrl() + VIDEO_STREAM_EXTENSION;
    }
    public remoteStatus() : IRemoteStatus {
        return this.currentRemoteStatus;
    }
    public async startRotation() : Promise<boolean> {
        if (this.restClient == null || this.currentRemoteStatus.currentRotating || !this.currentRemoteStatus.connected) {
            return false;
        }
        this.currentRemoteStatus.currentRotating = true;
        const response : rm.IRestResponse<any> = await this.restClient.get<any>(START_ROTATION_EXTENSION);
        if (response != null && response.statusCode === 200) {

            return true;
        }
        this.currentRemoteStatus.currentRotating = false;

        return false;
    }
    public async downloadPhotosFromCapture() : Promise<boolean> {
        if (this.restClient == null || this.currentRemoteStatus.currentDir === ''){

            return false;
        }

        console.log('Starting download');

        const picturesTotal : number = this.currentRemoteStatus.currentPicturesTotal;
        let picturesTaken : number = this.currentRemoteStatus.currentPicturesTaken;
        let downloadsQueued : number = 0;
        this.picturesDownloaded = 0;


        // Make photos root, if not present
        const rootDir : string = process.cwd() + IMAGE_ROOT_DIR;
        if (!fs.existsSync(rootDir)) {
            fs.mkdirSync(rootDir, {});
        }

        // Make sub-folder for current capture
        while (fs.existsSync(rootDir + IMAGE_SUBDIR_PREFIX + this.nextCaptureDirNum)) {
            this.nextCaptureDirNum++;
        }
        const subDir : string = rootDir + IMAGE_SUBDIR_PREFIX + this.nextCaptureDirNum;
        fs.mkdirSync(subDir, {});

        // Queue each photo for download
        while (downloadsQueued < picturesTotal) {
            console.log(this.picturesDownloaded, picturesTaken, picturesTotal);

            while (downloadsQueued >= picturesTaken) {
                const status: IRemoteStatus = await this.asyncFetchRemoteStatus();
                picturesTaken = status.currentPicturesTaken;
            }
            this.restClient.client.get(`${this.baseUrl()}${GET_PHOTO_EXTENSION}/${downloadsQueued}`).then(async (e : HttpClientResponse) => {
                if (this.picturesDownloaded < 3) {
                    fs.writeFileSync(subDir + '/' + this.picturesDownloaded + '.jpg', await e.readBody());
                }
                console.log(e);
                this.picturesDownloaded++;
            });

            // const image = new Image();
            // image.src = this.baseUrl() + GET_PHOTO_EXTENSION + '/' + downloadsQueued;
            // image.onload = () => {
            //     this.picturesDownloaded++;
            //     console.log(subDir);
            //     console.log(this);
            //     console.log(image);
            // };
            downloadsQueued++;
        }

        return true;
    }

    public async asyncFetchRemoteStatus() : Promise<IRemoteStatus> {
        if (this.restClient == null) {
            this.connectionError = true;

            return defaultRemoteStatus();
        }
        try {
            const response : rm.IRestResponse<any> =
                await this.restClient.get<any>(STATUS_EXTENSION);
            this.connectionError = false;
            this.pingConnected = (response != null && response.statusCode === 200);
            if (response != null && response.statusCode === 200) {
                this.pingConnected = true;
                this.currentRemoteStatus = toIRemoteStatus(response.result);
            } else {
                this.pingConnected = false;
                this.currentRemoteStatus = defaultRemoteStatus();
            }
            this.connectionError = false;
            if (response) {
                console.debug(response.statusCode, response.result);
            }
        } catch (e) {
            // tslint:disable-next-line:no-console
            console.error(e);
            this.connectionError = true;
            this.pingConnected = false;
            this.currentRemoteStatus = defaultRemoteStatus();
        }

        return this.currentRemoteStatus;
    }

    private baseUrl() : string {
        let ret : string = `${this.remoteHost}`;
        if (this.remotePort) {
            ret += `:${this.remotePort}`;
        }

        return ret;
    }

    private updateRestClient() : void {
        if (this.restClient != null) {
            this.restClient.client.dispose();
        }
        this.pingConnected = false;
        this.restClient = new rm.RestClient('aggregate-gui', this.baseUrl());
    }

}
