/**
 * React renderer.
 */
import {Typography} from '@material-ui/core';
import * as React from 'react';
import VideocamIcon from '@material-ui/icons/Videocam';
import VideocamOffIcon from '@material-ui/icons/VideocamOff';


export interface ICameraProps {
    img_src: string;
    enabled: boolean;
}

// export interface ICameraState { }

/**
 *  Camera Display Module
 */
export class CameraDisplay extends React.Component<ICameraProps, {}> {
    public render() : any {
        return (<div className='cameraDisplay'>
            <Typography variant='h5'>Live Camera</Typography>
            {this.props.enabled
                ? <img src={this.props.img_src} alt={'Camera Live Feed'}/>
                : <Typography variant={'subtitle1'}>No Connection</Typography>  }
        </div>);
    }
}
