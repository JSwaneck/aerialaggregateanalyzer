/**
 * React renderer.
 */
import {
    Button,
    ExpansionPanel,
    ExpansionPanelDetails,
    ExpansionPanelSummary, Grid,
    TextField,
    Typography
} from '@material-ui/core';
import ErrorIcon from '@material-ui/icons/Error';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import LoopIcon from '@material-ui/icons/Loop';
import WifiIcon from '@material-ui/icons/Wifi';
import WifiOffIcon from '@material-ui/icons/WifiOff';
import * as React from 'react';

export interface IConnectionProps {
    connectionStatus: boolean;
    remoteUrl: string;
    remotePort: string;
    connectionError: boolean;
    connectionPinging: boolean;
    onRemoteUpdated(newUrl: string, newPort: string): any;
}

export interface IConnectionState {
    unsavedRemoteUrl : string;
    unsavedRemotePort : string;
}

/**
 *  Remote Url Input
 */
export class ConnectionDisplay extends React.Component<IConnectionProps, IConnectionState> {

    public constructor(props : IConnectionProps) {
        super(props);
        this.state = {
            unsavedRemoteUrl: props.remoteUrl,
            unsavedRemotePort: props.remotePort
        };
        this.saveChanges = this.saveChanges.bind(this);
    }

    public render(): any {

        return <ExpansionPanel className={'gridConnection'}>
            <ExpansionPanelSummary expandIcon={<ExpandMoreIcon />}>
                <span className={'expansionPanelSummary'}>
                    <Typography variant='h5'>Connection</Typography>
                    {this.props.connectionStatus ? <WifiIcon/> : <WifiOffIcon/>}
                    {this.props.connectionError ? <ErrorIcon/> : null}
                    {this.props.connectionPinging ? <LoopIcon className={'spin'}/> : null}
                </span>
            </ExpansionPanelSummary>
            <ExpansionPanelDetails>
                <Grid container spacing={1} className={'connectionPanelDetails'} >
                    <Grid container item xs={12} sm={8}>
                        <TextField label={'Raspberry Pi IP/URL'}
                                   variant={'outlined'}
                                   value={this.state.unsavedRemoteUrl}
                                   className={'hostInput'}
                                   onChange={e => this.setState({unsavedRemoteUrl: e.target.value})}/>
                    </Grid>
                    <Grid container item xs={12} sm={2}>
                        <TextField label={'Port'}
                                   variant={'outlined'}
                                   value={this.state.unsavedRemotePort}
                                   className={'hostInput'}
                                   onChange={e => this.setState({unsavedRemotePort: e.target.value})}/>
                    </Grid>
                    <Grid container item xs={12} sm={2}>
                        <Button variant='contained'
                                color='primary'
                                disabled={this.changesSaved()}
                                className={'saveButton'}
                                onClick={this.saveChanges}>
                            Save
                        </Button>
                    </Grid>
                </Grid>
            </ExpansionPanelDetails>

        </ExpansionPanel>;
    }

    private saveChanges() : void {
        this.props.onRemoteUpdated(this.state.unsavedRemoteUrl, this.state.unsavedRemotePort);
    }

    private changesSaved() : boolean {
        return this.state.unsavedRemotePort === this.props.remotePort
            && this.state.unsavedRemoteUrl === this.props.remoteUrl;
    }
}
