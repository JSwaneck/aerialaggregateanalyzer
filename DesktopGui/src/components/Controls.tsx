/**
 * Controls component
 */
import {Connection} from '@/business/Connection';
import {IRemoteStatus} from '@/business/RemoteStatus';
import {Button, LinearProgress, Typography} from '@material-ui/core';
import * as React from 'react';

export interface IControlsProps {
    connection : Connection;
}
export interface IControlsState {
    requestingRotation : boolean;
    awaitingStartDownload : boolean;
}

/**
 * Controls component
 */
export class Controls extends React.Component<IControlsProps, IControlsState> {
    constructor(props : IControlsProps) {
        super(props);
        this.state = {
            requestingRotation: false,
            awaitingStartDownload: false
        };
        this.onStartRotation = this.onStartRotation.bind(this);
        this.onStartDownload = this.onStartDownload.bind(this);
    }

    public componentWillUpdate(prevProps: Readonly<IControlsProps>, prevState: Readonly<IControlsState>, snapshot?: any): void {
        if (this.state.requestingRotation && this.props.connection.remoteStatus().currentRotating) {
            this.setState({
                requestingRotation: false
            });
        }
        const picturesTaken : number = this.props.connection.remoteStatus().currentPicturesTaken;
        const picturesTotal : number = this.props.connection.remoteStatus().currentPicturesTotal;
        if (!this.props.connection.remoteStatus().currentRotating && picturesTaken === picturesTotal
            && !this.state.awaitingStartDownload && picturesTotal !== -1) {
            this.setState({
                awaitingStartDownload: true
            });
        }
    }

    public render() : any {
        const status : IRemoteStatus = this.props.connection.remoteStatus();

        const disableRotating : boolean = !status.connected || status.currentRotating;
        const disableProgress : boolean = !status.connected || !status.currentRotating
            || status.currentPicturesTotal < 1 || status.currentPicturesTaken === -1;
        const progressVal : number = status.currentPicturesTaken === -1 ? 0 :
            status.currentPicturesTaken / status.currentPicturesTotal * 100;
        const progressDownloadVal : number = status.currentPicturesTaken === -1 ? 0 :
            this.props.connection.getPicturesDownloaded() / status.currentPicturesTaken * 100;

        let statusMessage : string = '';
        if (!status.connected)
            statusMessage = 'Not connected';
        else if (status.currentRotating)
            statusMessage = 'Taking pictures...';
        else if (progressVal === 100)
            statusMessage = 'Complete! Ready to capture another.'
        else if (progressVal === -1)
            statusMessage = 'Ready to capture.';
        else
            statusMessage = 'N/A';

        return (<div className='controls'>
            <div className={'controlsContainer'}>
                <Typography variant='h5'>Capture</Typography>
                <Typography variant='body1'>
                    Rotates the tray and takes pictures, then downloads the results
                </Typography>
                <Typography variant={'caption'}>Status</Typography>
                <Typography variant={'body1'}>{statusMessage}</Typography>
                <Typography variant={'caption'}>
                    Photos Taken ({status.currentPicturesTaken} / {status.currentPicturesTotal})
                </Typography>
                <LinearProgress variant='determinate' value={progressVal}  />
                <Typography variant={'caption'}>
                    Photos Downloaded ({this.props.connection.getPicturesDownloaded()} / {status.currentPicturesTotal})
                </Typography>
                <LinearProgress variant='determinate' value={progressDownloadVal}  />
                <div>
                    <Button variant='contained'
                            color='primary'
                            disabled={disableRotating}
                            className={'saveButton'}
                            onClick={this.onStartRotation}>
                        Capture
                    </Button>
                    <Button variant='contained'
                            color='secondary'
                            disabled={!this.state.awaitingStartDownload}
                            className={'saveButton'}
                            onClick={this.onStartDownload}>
                        Download
                    </Button>
                </div>
            </div>
        </div>);
    }

    public onStartRotation() : void {
        this.setState({
            requestingRotation: true,
            awaitingStartDownload: false
        });
        this.props.connection.startRotation();
    }
    public onStartDownload() : void {
        this.setState({
            awaitingStartDownload: false
        });
        this.props.connection.downloadPhotosFromCapture();
    }
}
