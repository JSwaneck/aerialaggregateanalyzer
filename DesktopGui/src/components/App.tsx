/**
 * Main application
 */
import {Connection} from '@/business/Connection';
import {IRemoteStatus} from '@/business/RemoteStatus';
import {CameraDisplay} from '@/components/CameraDisplay';
import {ConnectionDisplay} from '@/components/ConnectionDisplay';
import {Controls} from '@/components/Controls';
import {Grid, Paper} from '@material-ui/core';
import * as React from 'react';

export interface IAppState {
    pinging: boolean;
}

const DEFUALT_REMOTE_HOST : string = 'http://192.168.7.108';
const DEFAULT_REMOTE_PORT : string = '5000';

/**
 *  Main Application React Component
 */
export class App extends React.Component<{}, IAppState> {
    private connection : Connection;
    private readonly interval : NodeJS.Timeout;

    public constructor(props: {}) {
        super(props);
        this.connection = new Connection(DEFUALT_REMOTE_HOST, DEFAULT_REMOTE_PORT);

        this.doPing = this.doPing.bind(this);
        this.interval = setInterval(this.doPing, 4000);

        this.state = {
            pinging: false
        };

        // bindings
        this.onRemoteConnectionUpdated = this.onRemoteConnectionUpdated.bind(this);
    }

    public render(): any {
        return <div>

           <Grid container spacing={1}>
               <Grid container item xs={12}>
                   <ConnectionDisplay connectionStatus={this.connection.connected()}
                                      connectionError={this.connection.invalidConnection()}
                                      connectionPinging={this.state.pinging}
                                      remoteUrl={this.connection.getRemoteHost()}
                                      remotePort={this.connection.getRemotePort()}
                                      onRemoteUpdated={this.onRemoteConnectionUpdated}/>
               </Grid>
               <Grid container item xs={12} sm={8}>
                   <Paper className={'gridPaper'}>
                       <CameraDisplay img_src={this.connection.getVideoStreamSrc()}
                                      enabled={this.connection.connected()}/>
                   </Paper>
               </Grid>
               <Grid container item xs={12} sm={4}>
                   <Paper className={'gridPaper'}>
                       <Controls connection={this.connection}/>
                   </Paper>
               </Grid>
           </Grid>
        </div>;
    }
    public  componentWillUnmount(): void {
        clearInterval(this.interval);
    }

    private async doPing() : Promise<void> {
        if (this.state.pinging) {
            return;
        }
        this.setState({pinging: true});
        const remoteStatus : IRemoteStatus = await this.connection.asyncFetchRemoteStatus();
        this.setState({pinging: false});
    }

    private onRemoteConnectionUpdated(newUrl : string, newPort: string) : void {
        this.connection.setRemote(newUrl, newPort);
        this.forceUpdate();
    }
}
