/**
 * React renderer.
 */
import {App} from '@/components/App';
import * as React from 'react';
import * as ReactDOM from 'react-dom';

// Import the styles here to process them with webpack
import 'typeface-roboto';

ReactDOM.render(
    <App/>,
    document.getElementById('app')
);

import '@public/style.css';
